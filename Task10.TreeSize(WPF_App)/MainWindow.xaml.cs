﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Task10.TreeSize_WPF_App_
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        int _contentFreeRowNumber = 0;
        int _drivesFreeRowNumber = 0;
        private bool _stopButton = false;
        static DirectoryInfo _dirInfo;
        private CatalogContent _catalog;
        private Thread interfaceThread;
        private Thread calculateThread;
        public MainWindow()
        {
            InitializeComponent();
            DriveInfo[] drives = DriveInfo.GetDrives();
            PrintDrives(drives);
            

        }

        public void CloseWindow(object sender, CancelEventArgs e)
        {
            if(interfaceThread != null)
            {
                CatalogContent.SetStopButton(true);
                _stopButton = true;
            }
            e.Cancel = false;
        }


        public void PrintDrives(DriveInfo[] drives)
        {

            foreach (var el in drives)
            {
                if(el.IsReady)
                {
                    Button myButton = new Button();
                    RowDefinition newRow = new RowDefinition();
                    Thickness thickness = new Thickness();
                    thickness.Bottom = 10;
                    myButton.Margin = thickness;
                    myButton.Content = el.RootDirectory;
                    myButton.HorizontalAlignment = HorizontalAlignment.Center;
                    myButton.VerticalAlignment = VerticalAlignment.Top;
                    myButton.Height = 20;
                    myButton.MinHeight = 16;
                    myButton.MinWidth = 40;
                    myButton.Background = new SolidColorBrush(Color.FromRgb(255, 255, 255));
                    myButton.Click += DriveClick;
                    myButton.ClickMode = ClickMode.Press;
                    CatalogContent catalogContent = new CatalogContent(myButton.Content.ToString());
                    Drives.RowDefinitions.Add(newRow);
                    Grid.SetRow(myButton, _drivesFreeRowNumber);
                    Grid.SetColumn(myButton, 0);
                    Drives.Children.Add(myButton);
                    TextBlock textBlock = new TextBlock();
                    textBlock.HorizontalAlignment = HorizontalAlignment.Center;
                    textBlock.VerticalAlignment = VerticalAlignment.Top;
                    textBlock.Margin = thickness;
                    textBlock.Text = catalogContent.ChoiceUnits(el.TotalSize);
                    Grid.SetRow(textBlock, _drivesFreeRowNumber);
                    Grid.SetColumn(textBlock, 1);
                    Drives.Children.Add(textBlock);
                    TextBlock newTextBlock = new TextBlock();
                    newTextBlock.Text = catalogContent.ChoiceUnits(el.AvailableFreeSpace);
                    newTextBlock.HorizontalAlignment = HorizontalAlignment.Center;
                    newTextBlock.VerticalAlignment = VerticalAlignment.Top;
                    newTextBlock.Margin = thickness;
                    Grid.SetRow(newTextBlock, _drivesFreeRowNumber);
                    Grid.SetColumn(newTextBlock, 2);
                    Drives.Children.Add(newTextBlock);
                    _drivesFreeRowNumber++;
                }
               

            }
        }

        private void DriveClick(object sender,RoutedEventArgs e)
        {
            _stopButton = true;
            CatalogContent.SetStopButton(true);
            int counter = 0;
            List<string> content = new List<string>();
            Button myButton = (Button)sender;
            _catalog = new CatalogContent(myButton.Content.ToString());
            content = _catalog.ReturnContnet();
            Content.Children.Clear();
            PrintContent(content);
            //GetDirsSize(myButton.Content.ToString(), catalog);
            InterfacePrint();
        }

        private async void PrintContent(List<string> content)
        {
            if (content.Any())
            {
                foreach (var el in content)
                {
                    Thickness thickness = new Thickness();
                    thickness.Bottom = 10;
                    Button myButton = new Button();
                    RowDefinition newRow = new RowDefinition();
                    myButton.Content = el.Length > 50 ? el.Substring(0,el.Length/2) + "..." : el ;
                    myButton.Tag = el;
                    myButton.HorizontalAlignment = HorizontalAlignment.Left;
                    myButton.VerticalAlignment = VerticalAlignment.Top;
                    myButton.Margin = thickness;
                    myButton.Height = 20;
                    myButton.MinHeight = 16;
                    myButton.MinWidth = 40;
                    myButton.Background = new SolidColorBrush(Color.FromRgb(255, 255, 255));
                    myButton.DataContext = false;
                    bool isFile = false;
                    if (Directory.Exists(el))
                    {
                        isFile = false;
                    }
                    Content.RowDefinitions.Add(newRow);
                    Grid.SetRow(myButton, _contentFreeRowNumber);
                    Grid.SetColumn(myButton, 0);
                    Content.Children.Add(myButton);
                    TextBlock textBlock = new TextBlock();
                    textBlock.HorizontalAlignment = HorizontalAlignment.Center;
                    textBlock.VerticalAlignment = VerticalAlignment.Top;
                    textBlock.Margin = thickness;
                    _dirInfo = new DirectoryInfo(el);
                    if (_dirInfo.Exists)
                    {
                        textBlock.Text = "Calculate";
                    }
                    else
                    {
                        textBlock.Text = "Calculate";
                    }
                    Grid.SetRow(textBlock, _contentFreeRowNumber);
                    Grid.SetColumn(textBlock, 1);
                    Content.Children.Add(textBlock);
                    TextBlock newTextBlock = new TextBlock();
                    newTextBlock.HorizontalAlignment = HorizontalAlignment.Center;
                    newTextBlock.VerticalAlignment = VerticalAlignment.Top;
                    FileInfo newInfo = new FileInfo(el); if (_dirInfo.Exists)
                    {
                        newTextBlock.Text = _dirInfo.LastWriteTime.ToString();

                    }
                    else
                    {

                        FileInfo info = new FileInfo(el);
                        newTextBlock.Text = info.LastWriteTime.ToString() ;
                    }
                    Grid.SetRow(newTextBlock, _contentFreeRowNumber);
                    Grid.SetColumn(newTextBlock, 2);
                    newTextBlock.Margin = thickness;
                    Content.Children.Add(newTextBlock);
                    _contentFreeRowNumber++;

                }
            }

            _contentFreeRowNumber = 0;
        }

        private async Task GetDirsSize(string sourceDir,CatalogContent catalog)
        {/*
            int counterToRemove = 1;
            int rowCounter = 0;
            foreach (var el in catalog.ReturnContnet())
            {
                long size = 0;
                await Task.Run(() => size = catalog.GetDirSize(el);
                Thickness thickness = new Thickness();
                thickness.Bottom = 10;
                TextBlock textBlock = new TextBlock();
                textBlock.HorizontalAlignment = HorizontalAlignment.Center;
                textBlock.VerticalAlignment = VerticalAlignment.Top;
                textBlock.Margin = thickness;
                Grid.SetColumn(textBlock,1);
                Grid.SetRow(textBlock,rowCounter);
                Content.Children.RemoveAt(counterToRemove);
                textBlock.Text = catalog.ChoiceUnits(size);
                Content.Children.Insert(counterToRemove,textBlock);
                counterToRemove+=3;
                rowCounter++;
            }*/

        }

        private void InterfacePrint()
        {
            _stopButton = false;
            CatalogContent.SetStopButton(false);
            interfaceThread = new Thread( () =>
            {
                calculateThread = new Thread(() =>
                {
                    _catalog.CalculateDirSize();
                });
                calculateThread.Start();
                int counterToRemove = 1;
                int rowCounter = 0;
                int counter = 0;
                while (!_stopButton)
                {
                    List<string> content = _catalog.ReturnContnet();
                    Dictionary<string, long> sizeAndContent = _catalog.GetDictionary();
                    Dispatcher.Invoke(() =>
                    {
                        if (counter >= content.Count)
                        {
                            counterToRemove = 1;
                            rowCounter = 0;
                            counter = 0;
                        }
                        long size = sizeAndContent[content[counter]];
                        counter++;
                        Thickness thickness = new Thickness();
                        thickness.Bottom = 10;
                        TextBlock textBlock = new TextBlock();
                        textBlock.HorizontalAlignment = HorizontalAlignment.Center;
                        textBlock.VerticalAlignment = VerticalAlignment.Top;
                        textBlock.Margin = thickness;
                        Grid.SetColumn(textBlock, 1);
                        Grid.SetRow(textBlock, rowCounter);
                        Content.Children.RemoveAt(counterToRemove);
                        textBlock.Text = _catalog.ChoiceUnits(size);
                        Content.Children.Insert(counterToRemove, textBlock);
                        counterToRemove += 3;
                        rowCounter++;
                    });
                    Thread.Sleep(100);
                }
                

            });
            interfaceThread.Start();
            
        }

    }
}
