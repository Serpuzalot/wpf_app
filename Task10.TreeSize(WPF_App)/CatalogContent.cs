﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Task10.TreeSize_WPF_App_
{
    class CatalogContent
    {
        private List<string> _allContent;
        private Dictionary<string, long> _contentAndSize;
        private string _path;
        private static bool _stopButton ;

        public CatalogContent(string path)
        {
            _allContent = new List<string>();
            _contentAndSize = new Dictionary<string, long>();
            this._path = path;
            GetContent();
        }

        public Dictionary<string, long> GetDictionary()
        {
            return _contentAndSize;
        }


        public void CalculateDirSize()
        {
            foreach (var path in _contentAndSize.Keys)
            {
                if (_stopButton)
                {
                    return;
                }
                (new Thread(() =>
                {
                    long size = 0;
                    string[] fileEntries = new string[0];
                    try
                    {
                        fileEntries = Directory.GetFiles(path);
                    }
                    catch
                    {
                        fileEntries = null;
                    }
                    if (fileEntries != null)
                    {
                        foreach (string fileName in fileEntries)
                        {
                            Interlocked.Add(ref size, (new FileInfo(fileName)).Length);
                        }

                        _contentAndSize[path] = size;
                    }

                    string[] subDirs = new string[0];
                    try
                    {
                        subDirs = Directory.GetDirectories(path);
                    }
                    catch
                    {
                        subDirs = null;
                    }
                    finally
                    {
                        if (subDirs != null)
                        {
                            foreach (var dir in subDirs)
                            {
                                CalculateDirSizeAsync(path, dir);
                            }

                        }
                    }
                })).Start();


            }
        }

        private async Task CalculateDirSizeAsync(string rootPath, string path)
        {
            long size = 0;
            string[] fileEntries = new string[0];
            if (_stopButton)
            {
                return;
            }
            try
            {
                fileEntries = Directory.GetFiles(path);
            }
            catch
            {
                fileEntries = null;
            }
            if (fileEntries != null)
            {
                foreach (string fileName in fileEntries)
                {
                    Interlocked.Add(ref size, (new FileInfo(fileName)).Length);
                }

                _contentAndSize[rootPath] += size;
            }
            string[] subDirs = new string[0];
            if (_stopButton)
            {
                return;
            }
            try
            {
                subDirs = Directory.GetDirectories(path);
            }
            catch
            {
                subDirs = null;
            }
            finally
            {
                if (subDirs != null)
                {
                    foreach (var dir in subDirs)
                    {
                        CalculateDirSizeAsync(rootPath, dir);
                    }
                }
            }
        }

        public string ChoiceUnits(long size)
        {
            int choice = 0;
            for(int i = 4; i > 0; i--)
            {
                if (size / Math.Pow(1024, i) > 1)
                {
                    choice = i;
                    break;
                }
            }
            switch (choice)
            {
                case 0:
                    return ((int)size).ToString() + "B";
                    break;
                case 1:
                    return ((int)(size / 1024)).ToString() + "Kb";
                    break;
                case 2:
                    return ((int)(size / Math.Pow(1024, 2))).ToString() + "Mb";
                    break;
                case 3:
                    return ((int)(size / Math.Pow(1024, 3))).ToString() + "Gb";
                    break;
                case 4:
                    return ((int)(size / Math.Pow(1024, 4))).ToString() + "Tb";
                    break;
                default:
                    return "Nun";
                    break;
            }
        }

        private void GetContent()
        {
            List<string> content = new List<string>();
            try
            {
                content.AddRange(Directory.EnumerateFileSystemEntries(_path).Where(n=>FileAttributes.System != (File.GetAttributes(n) & FileAttributes.System)));

            }
            catch
            {
                content = null;
            }
            finally
            {
                if (content != null && content.Count != 0)
                {
                    _allContent.AddRange(content);
                    foreach(var el in _allContent)
                    {
                        _contentAndSize.Add(el, 0);
                    }
                }
            }

        }

        public override string ToString()
        {
            string result = "Result:\n";
            foreach (var el in _allContent)
            {
                result += el + "\n";
            }
            return result;
        }

        public List<string> ReturnContnet()
        {
            return _allContent;
        }

        public static void SetStopButton(bool value)
        {

            _stopButton = value;
        }
    }
}
